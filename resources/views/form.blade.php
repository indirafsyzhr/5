<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=<device-width>, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        {{ csrf_field() }}
        <label for="First_Name">First Name :</label><br><br>
        <input type="text" id="First_Name" name="First_Name"><br><br>
        <label for="Last_Name">Last Name : </label><br><br>
        <input type="text" id="Last_Name" name="Last_Name"><br><br>

        <label for="gender">Gender : </label><br><br>
        <input type="radio" id="male" value="Male" name="gender">
        <label for="male">Male </label><br>
        <input type="radio" id="female" value="Female" name="gender">
        <label for="female">Female </label><br>
        <input type="radio" id="other" value="other" name="gender">
        <label for="other">Other</label><br><br>

        <label for="nationality">Nationality</label><br><br>
        <select>
            <option value="indonesia">Indonesia</option>
            <option value="singapoer">Singapoer</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Australia">Australia</option>
        </select><br><br>

        <label for="language">Language Spoken: </label><br><br>
        <input type="checkbox" id="b_indo" value="bahasaindonesia" name="gender">
        <label for="bahasaindonesia">Bahasa Indonesia </label><br>
        <input type="checkbox" id="english" value="english" name="gender">
        <label for="english">English </label><br>
        <input type="checkbox" id="other" value="other" name="gender">
        <label for="other">Other</label><br><br>

        <label for="bio">Bio:</label><br><br>
        <textarea id="bio" rows="5" cols="30"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>